package listasEnlazadas;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

public class Ejecutable3 {
    public static void main(String[] args) {
        // 1) ver --> tranforma de xml --> json (viene desornado); --> resolvimos ordenado

        // 2) core (ordenado) --> mantenerlo ordenado
        // Create a new ordered map.
        LinkedHashMap<String, String> myLinkedHashMap = new LinkedHashMap<>();
        // Add items, in-order, to the map.
        myLinkedHashMap.put("A", "first");
        myLinkedHashMap.put("Z", "second");
        myLinkedHashMap.put("D", "third");
        myLinkedHashMap.put("Y", "four");
        myLinkedHashMap.put("C", "third");
        myLinkedHashMap.put("B", "third");
        System.out.println(printJSONObject().toString(4));
    }

    /****/
    public static JSONObject printJSONObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            Field changeMap = jsonObject.getClass().getDeclaredField("map");
            changeMap.setAccessible(true);
            changeMap.set(jsonObject, new LinkedHashMap<>());
            changeMap.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            System.out.println(e.getMessage());
            return null;
        }
        jsonObject.put("A", "first");
        jsonObject.put("Z", "second");
        jsonObject.put("D", "third");
        jsonObject.put("Y", "four");
        jsonObject.put("C", "third");
        jsonObject.put("B", "third");
        return jsonObject;
    }
}
