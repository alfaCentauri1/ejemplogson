package listasEnlazadas;

import com.fasterxml.jackson.databind.util.JSONPObject;
import netscape.javascript.JSObject;
import org.json.JSONObject;

import java.util.*;

public class Ejecutable2 {
    public static void main(String[] args) {
        // Create a new ordered map.
        Map<String, String> myLinkedHashMap = new LinkedHashMap<>(16, 0.75f, false);

        // Add items, in-order, to the map.
        myLinkedHashMap.put("A", "first");
        myLinkedHashMap.put("F", "second");
        myLinkedHashMap.put("D", "third");
        myLinkedHashMap.put("E", "four");
        myLinkedHashMap.put("C", "third");
        myLinkedHashMap.put("B", "third");
        //
        System.out.println(myLinkedHashMap.toString());
        // Instantiate
        JSONObject jsonObject = new JSONObject(myLinkedHashMap);
        String resultado = jsonObject.toString();
        System.out.println(resultado);
    }
}
