package listasEnlazadas;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Ejecutable4 {

    /**/
    public static void main(String[] args) {
        Map myLinkedHashMap = new LinkedHashMap<>();
        Gson gson = new Gson();
        System.out.println("Programa de ejemplo para crear un json con GSon.");
        myLinkedHashMap.put("nombre", "Gato con botas");
        myLinkedHashMap.put("edad", "3");
        String[] arrayComidas = {"Leche", "Pollo", "Atun"};
        myLinkedHashMap.put("comidas", arrayComidas);
        Map adress = new HashMap();
        adress.put("calle", "Paraguay");
        adress.put("numero", "610");
        myLinkedHashMap.put("direccion",adress);
        myLinkedHashMap.put("estadoActivado", true);
        myLinkedHashMap.put("estadoEspera", false);
        myLinkedHashMap.put("cantidad", 15);
        myLinkedHashMap.put("telefono", 54112345678L);
        myLinkedHashMap.put("precio",34.5);
        System.out.println(myLinkedHashMap);
        Object mapa = myLinkedHashMap.get("direccion");
        if( mapa instanceof Map) {
            System.out.println("Es un mapa " + mapa);
        }
        Object arreglo = myLinkedHashMap.get("comidas");
        if( arreglo instanceof String[]) {
            System.out.println("Es un arreglo " + arreglo);
            int max = ((String[]) arreglo).length;
            for (Object element : (String[])arreglo){
                System.out.println("Elemento # " + element);
            }
        }
        //
        System.out.println("Fin del ejemplo.");
    }
}
